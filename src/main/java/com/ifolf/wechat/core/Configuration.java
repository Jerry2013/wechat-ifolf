package com.ifolf.wechat.core;

import com.ifolf.wechat.utils.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * <p>
 *     微信公众帐号配置文件
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-03-26 00:03
 * @since JDK 1.6
 */
public class Configuration {
    protected Logger logger = LoggerFactory.getLogger(Configuration.class);

    public static String APPID;
    public static String APPSECRET;
    public static String TOKEN;
    public static String MENU;

    public static final String WECHAT_APPID = "wechat.appid";
    public static final String WECHAT_APPSECRET = "wechat.appsecret";
    public static final String WECHAT_MENU = "wechat.menu";
    public static final String WECHAT_TOKEN = "wechat.token";
    public static final String WECHAT_PROCESSOR = "wechat.processor";

    public final static Configuration CONFIGURATION = new Configuration();

    private Properties properties;


    private Configuration() {

    }

    /**
     * 初始化配置
     *
     * @param configuration
     */
    public void init(String configuration) {
        if (logger.isInfoEnabled()) {
            logger.info("read config from {}", configuration);
        }
        properties = new Properties();
        FileUtil.loadProperties(configuration, properties);

        APPID = (String) properties.get(WECHAT_APPID);
        APPSECRET = (String) properties.get(WECHAT_APPSECRET);
        TOKEN = (String) properties.get(WECHAT_TOKEN);
        MENU = (String)  properties.get(WECHAT_MENU);
    }

    public String get(String key) {
        return (String) properties.get(key);
    }

}
