package com.ifolf.wechat.core;

import com.ifolf.wechat.api.Cache;
import com.ifolf.wechat.processor.Processor;
import com.ifolf.wechat.message.OutputMessage;
import com.ifolf.wechat.utils.MessageUtil;
import com.ifolf.wechat.utils.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * <p>
 *  微信服务器与本地程序交互的入口
 *  GET方式用于TOKEN
 *  POST方式用于消息相互发送
 *
 *  ifolf
 *  http://git.oschina.net/suninformation/ymate-module-wechat
 *  wechat-ifolf
 *
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-03-25 23:03
 * @since JDK 1.6
 */
//@WebServlet(name = "wechat", loadOnStartup = 1, urlPatterns= {"/wechat"}, asyncSupported = true, initParams = {@WebInitParam(name = "configuration", value = "wechat.properties")})
public class Bootstarp extends HttpServlet {
    private static final long serialVersionUID = -8392350177385686099L;
    private Logger logger = LoggerFactory.getLogger(Bootstarp.class);
    private Configuration config = Configuration.CONFIGURATION;

    private Processor processor;

    @Override
    public void init() throws ServletException {
        String configuration = getInitParameter("configuration");
        config.init(configuration);
        String className = config.get(Configuration.WECHAT_PROCESSOR);
        createProcessor(className);

        //启动缓存池
        Cache cache = Cache.getInstance();
        cache.setRunning(true);
        new Thread(cache).start();
    }

    private void createProcessor(String configClass) {
        if (configClass == null)
            throw new RuntimeException("Please set processor class");

        try {
            Object temp = Class.forName(configClass).newInstance();
            if (temp instanceof Processor)
                processor = (Processor)temp;
            else
                throw new RuntimeException("Can not create instance of class: " + configClass);
        } catch (InstantiationException e) {
            throw new RuntimeException("Can not create instance of class: " + configClass, e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Can not create instance of class: " + configClass, e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found: " + configClass, e);
        }
    }

    /**
     * 验证URL有效性
     *
     * 开发者提交信息后，微信服务器将发送GET请求到填写的URL上，GET请求携带四个参数：
     *  signature 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
     *  timestamp 时间戳
     *  nonce 随机数
     *  echostr 随机字符串
     *
     * 开发者通过检验signature对请求进行校验（下面有校验方式）。
     * 若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，成为开发者成功，否则接入失败。
     *
     * 加密/校验流程如下：
     *  1. 将token、timestamp、nonce三个参数进行字典序排序
     *  2. 将三个参数字符串拼接成一个字符串进行sha1加密
     *  3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
     *
     * @param req
     * @param resp
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String signature = req.getParameter("signature");	//微信加密签名
        String timestamp = req.getParameter("timestamp"); 	//时间戳
        String nonce = req.getParameter("nonce");	//随机数
        String echostr = req.getParameter("echostr");//认证字符串

        //TOKEN配置文件中读取
        if (SecurityUtil.checkSignature(config.TOKEN, signature, timestamp, nonce)) {
            if (logger.isInfoEnabled()) {
                logger.info("Token authentication is successful");
            }
            resp.getWriter().write(echostr);
        }
    }

    /**
     * 微信消息处理入口
     *
     * @param req
     * @param resp
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/xml;charset=utf-8");

        try {
            PrintWriter out = resp.getWriter();
            //解析微信服务器推送的消息
            Map<String,String> map = MessageUtil.parseXml(req);
            if (logger.isInfoEnabled()) {
                logger.info("The message is：{}", map);
            }
            OutputMessage respBaseMessage = processor.process(map);
            responseMessage(out, respBaseMessage);
            out.flush();
        } catch (Exception e) {
            logger.error("process message error", e);
        }
    }

    private void responseMessage(PrintWriter out, OutputMessage outputMessage) {
        if (outputMessage != null) {
            String xml = MessageUtil.outputMessageToXml(outputMessage);
            out.print(xml);
        }
    }
}
