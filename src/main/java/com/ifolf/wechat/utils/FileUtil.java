package com.ifolf.wechat.utils;

import com.google.common.io.ByteSource;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * <p>
 *  文件操作工具类
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-03-26 00:03
 * @since JDK 1.6
 */
public class FileUtil {

    /**
     * 读取properties配置文件
     *
     * @param file
     * @param properties
     */
    public static void loadProperties(String file, Properties properties){
        URL url = Resources.getResource(file);
        if (url == null) {
            throw new IllegalArgumentException("Parameter of file can not be blank");
        }
        ByteSource byteSource = Resources.asByteSource(url);
        try {
            properties.load(byteSource.openStream());
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file can not be loading: " + url);
        }
    }

}
