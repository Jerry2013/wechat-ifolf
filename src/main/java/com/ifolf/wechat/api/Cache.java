package com.ifolf.wechat.api;

import com.google.common.collect.Maps;
import com.ifolf.wechat.api.beans.Expires;

import java.util.Map;
import java.util.Set;

/**
 * <p>
 * .
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-02-12 13:41
 * @since JDK 1.6
 */
public class Cache implements Runnable {
    public final static String ACCESS_TOKEN = "access_token";

    private byte[] LOCK = new byte[0];
    public Map<String, Expires> pool = Maps.newHashMap();
    public boolean running;

    private static Cache instance = new Cache();

    private Cache() {}

    public static Cache getInstance() {
        return instance;
    }

    /**
     * 向缓存池中设置对象
     *
     * @param key
     * @param expires
     * @return
     */
    public Expires set(String key, Expires expires) {
        synchronized (LOCK) {
            return pool.put(key, expires);
        }
    }

    /**
     * 从缓存池中获取对象
     *
     * @param key
     * @return
     */
    public Expires get(String key) {
        synchronized (LOCK) {
            return pool.get(key);
        }
    }


    @Override
    public void run() {
        while (running) {
            synchronized (LOCK) {
                Set<Map.Entry<String, Expires>> set = pool.entrySet();
                for (Map.Entry<String, Expires> entry : set) {
                    Expires e = entry.getValue();
                    long access = e.getAccess() / 1000;
                    long in = e.getExpires_in();
                    long cur = System.currentTimeMillis() / 1000;
                    if ((in - (cur - access)) <= 10) {
                        pool.remove(entry.getKey());
                    }
                }
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
