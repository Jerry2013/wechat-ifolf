package com.ifolf.wechat.api.beans;

/**
 * <p>
 * .
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-03-27 23:03
 * @since JDK 1.6
 */
public class MenuButtonBeanHelp {
    private MenuButton menu;

    public MenuButton getMenu() {
        return menu;
    }

    public void setMenu(MenuButton menu) {
        this.menu = menu;
    }
}
