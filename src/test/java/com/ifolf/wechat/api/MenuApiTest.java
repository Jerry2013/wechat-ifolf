package com.ifolf.wechat.api;

import com.ifolf.wechat.api.beans.ErrCodeMsg;
import com.ifolf.wechat.api.beans.MenuButton;
import org.junit.Test;

/**
 * <p>
 * . fancydong
 * </p>
 *
 * @author Jerry Ou
 * @version 1.0 2014-02-12 13:41
 * @since JDK 1.6
 */
public class MenuApiTest {
    private String menu = "{\n" +
            "     \"button\":[\n" +
            "     {\t\n" +
            "          \"type\":\"click\",\n" +
            "          \"name\":\"今日歌曲\",\n" +
            "          \"key\":\"V1001_TODAY_MUSIC\"\n" +
            "      },\n" +
            "      {\n" +
            "           \"type\":\"click\",\n" +
            "           \"name\":\"歌手简介\",\n" +
            "           \"key\":\"V1001_TODAY_SINGER\"\n" +
            "      },\n" +
            "      {\n" +
            "           \"name\":\"菜单\",\n" +
            "           \"sub_button\":[\n" +
            "           {\t\n" +
            "               \"type\":\"view\",\n" +
            "               \"name\":\"搜索\",\n" +
            "               \"url\":\"http://www.soso.com/\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"type\":\"view\",\n" +
            "               \"name\":\"视频\",\n" +
            "               \"url\":\"http://v.qq.com/\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"type\":\"click\",\n" +
            "               \"name\":\"赞一下我们\",\n" +
            "               \"key\":\"V1001_GOOD\"\n" +
            "            }]\n" +
            "       }]\n" +
            " }";


    @Test
    public void testCreateMenu() {
       ErrCodeMsg s = MenuApi.createMenu(menu, AccessTokenApi.getAccessToken(DefaultApp.AppId, DefaultApp.AppSecret).getAccess_token());
       System.out.println(s.getErrcode() + "===" + s.getErrmsg());
    }

    @Test
    public void testGetMenu() {
        MenuButton menu = MenuApi.getMenu(AccessTokenApi.getAccessToken(DefaultApp.AppId, DefaultApp.AppSecret).getAccess_token());
        System.out.println(menu.toJSONString());
    }

    @Test
    public void testDeleteMenu() {
        ErrCodeMsg s = MenuApi.deleteMenu(AccessTokenApi.getAccessToken(DefaultApp.AppId, DefaultApp.AppSecret).getAccess_token());
        System.out.println(s.getErrcode() + "===" + s.getErrmsg());
    }
}
